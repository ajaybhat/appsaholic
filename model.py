from sqlalchemy import create_engine

def execute_query(query):
    engine = create_engine('sqlite:///appsaholic.db')
    connection = engine.connect()
    results = connection.execute(query)
    connection._commit_impl(autocommit=True)
    return results


